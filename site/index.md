<img class="right" alt="Image of Jochen Sprickerhof" src="js.jpg" width="200" height="216">

# Address

Jochen Sprickerhof  
Veitsmüllerweg 15  
85354 Freising  
eMail: web<span style="display:none">-anti-spam</span>@jochen.sprickerhof.de  
GnuPG Key: [EC47 84C5 B085 9A9A 9FC5  FCF0 17FB DCBF D992 8FF4](gpg_key_jochen.asc)

# Fields of interest

  - Open Source Software
  - Debian
  - F-Droid
  - ROS
  - Sensor Data Interpretation
  - Autonomous Mobile Robots
  - Artificial Intelligence
