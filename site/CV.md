# Employment

=2021 - today=
    **Freelancer**, *Work on F-Droid, Debian and other open source software.*, Worldwide.
=2016 - 2021=
    **Senior Robot Software Developer**, *[Magazino](https://magazino.eu)*, Germany.
=2012 - 2017=
    **Research associate**, *[Osnabrück University](https://www.uni-osnabrueck.de)*, Germany.  
    Knowledge-Based Systems group
=2015=
    **Scientific advisor**, *Caspian Robotics*, Germany.
=2009 - 2012=
    **Research associate**, *Osnabrück University*, Germany.  
    Knowledge-Based Systems group, Lichtenberg scholarship
=May - September 2009=
    **Student assistant [TTWISS](https://www2.inf.uni-osnabrueck.de/kbs/ttwiss.html)**, *Osnabrück University*, Germany.  
    Clusters of excellence "aviation cluster metropolitan area Hamburg"
=2004 - 2009=
    **Teaching assistant**, *Osnabrück University*, Germany.  
    Tutor for various lectures. For details see Teaching
=1999 - 2012=
    **System administrator**, *Zinzendorfschule Tossens*, Germany.  
    Responsible for Linux terminal server, terminals, mail server, print server, network, website
=2001 - 2002=
    **Civilian Service**, *CVJM Sozialwerk Nordenham*, Germany.


# Education

=October 2009 - March 2017=
    **PhD student**, *Osnabrück University*, Germany.  
    Member of the Research Training Group [Adaptivity in Hybrid Cognitive Systems](https://www.ikw.uni-osnabrueck.de/en/students/phd_programs.html),
Topic: Closed-Loop interpretation of sub-symbolic 3D laser scan data
(Publications: KogWis, Automatika, 3D-Tage, ARSO, KI, IAV)

=2009=
    **[Diploma](https://jochen.sprickerhof.de/Software/ELCH/) in applied system science**, *Osnabrück University*, Germany.  
    Thesis topic: efficient loop closing using six degrees of freedom in laser scans from mobile robots.
(Publications: ECMR 2009, Automatika 2011)

=2002 - 2009=
    **Student**, *Osnabrück University*, Germany.  
    Studies of [applied system science](https://www.usf.uni-osnabrueck.de),
subjects: computer science, mathematics, system science, physics, cognitive science

=2001=
    **Abitur**, *[Zinzendorfschule Tossens](https://www.zinzendorfschule.de)*, Germany.  
    German school-leaving exam and university entrance qualification.
Main subjects: mathematics, physics

=1994 - 2001=
    **High School**, *Zinzendorfschule Tossens*, Germany.  
    Grammar school
=1992 - 1994=
    **Junior High School**, *Orientierungsstufe Tossens*, Germany.  
    Intermediate school
=1988 - 1992=
    **Primary School**, *Grundschule Burhave*, Germany.
=1987 - 1988=
    **Preschool**, *Vorschule Burhave*, Germany.


# Projects and Internships

=Oct 2019 - today=
    **F-Droid Developer**, *[F-Droid](https://www.f-droid.org)*, Internet.
=Sep 2016 - today=
    **Etar maintainer**, *[Etar](https://www.github.com/Etar-Group/Etar-Calendar)*, Internet.
=May 2015 - today=
    **Debian Developer**, *[Debian](https://www.debian.org)*, Internet.  
    [Maintainer](https://qa.debian.org/developer.php?login=Jochen+Sprickerhof) of ROS, PCL, OpenNI
=February 2013 - today=
    **Scientist**, *[Open Perception](https://web.archive.org/web/20191228085834/http://openperception.org/)*, Internet.
=March 2011 - today=
    **Developer and maintainer**, *[Point Cloud Library (PCL)](https://pointclouds.org)*, Internet.
=Jan 2008 - today=
    **Developer**, *[SLAM6D](https://slam6d.sourceforge.io)*, Internet.
=May - October 2014=
    **Project supervisor**, *Osnabrück University*, Germany.  
    Developing a robot for the SICK robot day 2014
=April 2013 - April 2014=
    **Project supervisor**, *Osnabrück University*, Germany.  
    [Muffin](https://www2.informatik.uni-osnabrueck.de/kbs/muffin/) study project
=January - October 2012=
    **Project supervisor**, *Osnabrück University*, Germany.  
    Developing a robot for the SICK robot day 2012
=July 2012=
    **Visiting researcher**, *Wuhan University*, China.  
    VeloSLAM project, DAAD exchange program
=April - July 2011=
    **Internship**, *[Willow Garage](https://web.archive.org/web/20200521180701/http://www.willowgarage.com/)*, USA.  
    Working with Radu Rusu on registration and mapping in the PCL
=March 2006=
    **Study Project**, *TU Dresden*, Germany.  
    Topic: simulation of a self organizing traffic light


# Awards and Competitions

=October 2012=
    **SICK robot day**, *Waldkirch*, Germany.  
    Special award
=February 2010=
    **Intevation award for Free Software**, *Osnabrück University*, Germany.  
    For the development of SLAM6D.
Together with Kai Lingemann, Andreas Nüchter, Dorit Borrmann and Jan Elseberg

=October 2007=
    **SICK robot day**, *Waldkirch*, Germany.  
    Second place in the indoor competition
=July 2005=
    **RoboCup**, *Osaka*, Japan.  
    Member of the Team "Deutschland 1"
=1995=
    **Charlotte Bernhard award**, *Zinzendorfschule Tossens*, Germany.  
    For the model of a space robot
=1994=
    **Charlotte Bernhard award**, *Zinzendorfschule Tossens*, Germany.  
    For the model of an autonomous rod storage


# Supervised thesis

=January 2017=
    **Bachelor thesis Oleg Postovyi**, *Osnabrück University*, Germany.  
    Evaluation of ROS v.2
=January 2016=
    **Master thesis Sebastian Pütz**, *Osnabrück University*, Germany.  
    3D Mapping and Navigation in Rough Terrain.
=November 2014=
    **Master thesis Florian Otte**, *Osnabrück University*, Germany.  
    3D-Features to Aid Scan Registration
=September 2014=
    **Master thesis Michael Görner**, *Osnabrück University*, Germany.  
    Autonomous Tabletop Object Learning
=September 2014=
    **Master thesis Ann-Katrin Häuser**, *Osnabrück University*, Germany.  
    Inferring Hidden World Parameters by Analyzing Observed Environment Changes.
=September 2014=
    **Bachelor thesis Michael Stypa**, *Osnabrück University*, Germany.  
    Detection of Colored Balls in Color and Depth Images
=December 2011=
    **Bachelor thesis Florian Otte**, *Osnabrück University*, Germany.  
    Automated Detection of Scan Shadows in 3D Laser Scans (together with Johannes Zimmermann).
=November 2010=
    **Bachelor thesis Johannes Zimmermann**, *Osnabrück University*, Germany.  
    Automated Detection of Scan Shadows in 3D Laser Scans (together with Florian Otte).
=September 2010=
    **Bachelor thesis Stefan Möbus**, *Osnabrück University*, Germany.  
    Determining Surface Material Properties by Evaluating Reflectance Values of the Laser Measurement System SICK LMS 100


# Teaching

=March 2019=
    **IK 2019**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/),
focus theme: "Out of your Senses".
Lecture series "Insights into your Robot - Seeing the World from their Perspective"

=winter term 2016/17=
    **Computer science lecturer**, *Osnabrück University*, Germany.  
    Exercises training [introduction to artificial intelligence](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=a1e12c91d9fd56a112b78d1f97cdfe0f),
seminar [AI and robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=672b52a5ec795c867570b754ee35dde8),
hands-on training [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=b23e811c43358190d5fb8d910fdeeac7).

=sumer term 2016=
    **Computer science lecturer**, *Osnabrück University*, Germany.  
    Exercises training [knowledge based systems](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=28ea00a41e4a5e970f4d75b29f562336),
Project [SICK-Robot-Day 2016](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=b1237e30f9c6932c1e06e933624827d2),
hands-on training [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=b1237e30f9c6932c1e06e933624827d2).

=March 2016=
    **IK 2016**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/),
focus theme: "Transitions and Transformations in Cognition, Biology, and Interactive Systems".
lecture series "Hands-On Mobile Robotics"

=winter term 2015/16=
    **Computer science lecturer**, *Osnabrück University*, Germany.  
    Exercises training [introduction to artificial intelligence](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=368d2a5bd0a54181faa9f940216e0d42),
seminar [AI and robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=893ae90cc1c5736ecc1fdbe0019b25ff),
hands-on training [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=5211bfdd10fd9c977de3b8b9df2861ec).

=summer term 2015=
    **Computer science lecturer**, *Osnabrück University*, Germany.  
    Exercises training [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=5ca280266c0a5bad06c7b8377330b060),
hands-on training [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=9b7ba6c1ddb9d0a670f2a40f66efff57).

=winter term 2014/15=
    **Computer science lecturer**, *Osnabrück University*, Germany.  
    Exercises training [introduction to artificial intelligence](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=db8056dd7cbd0d48508778ec20e161d0),
seminar [knowledge based systems](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=b5706f1cb7a7262861ce2e8f325e569e),
hands-on training [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=da8459b710a5a911526ce5d14ca08d7c).

=summer term 2014=
    **Computer science lecturer**, *Osnabrück University*, Germany.  
    Project [SICK-Robot-Day 2014](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=daeb3f5092639823117f0935b15c9f9c),
lecture [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=f186d7f8ea63e8d03392a1fe4eceea7c),
hands-on training [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=3153cffcb1dc4ec54a3e8142a3c19b9b).

=winter term 2013/14=
    **Computer science lecturer**, *Osnabrück University*, Germany.  
    Study group [MUFFIN](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=22c3df521a5a57d9f9dd8d499a754449),
reading group [mobile manipulation and semantic mapping](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=9e8c3c99d9bbdada1892031b8562ac2e),
hands-on training [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=7c8b6fb15f20796bd7e72f1673f0991f).

=summer term 2013=
    **Computer science lecturer**, *Osnabrück University*, Germany.  
    Study group [MUFFIN](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=22c3df521a5a57d9f9dd8d499a754449),
seminar [AI and robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=9201fb695b6865f4032c51c531cd0d34),
hands-on training [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=8e7ba62a90d9deba7ccd621f1ab9d8e2).

=winter term 2012/13=
    **Computer science lecturer**, *Osnabrück University*, Germany.  
    Exercises training [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=e5505ce525114124d697b98e7e4e55a6),
reading group [AI and robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=00fbf3a9071513c6fc2af562092958a5),
hands-on training [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=9a2bf583bc4bdc364bb5c1e2136b690b).

=summer term 2012=
    **Computer science lecturer**, *Osnabrück University*, Germany.  
    Seminar [operating systems](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=b0e0083160330d8c94d7976fcc1bfa2b),
hands-on training [SICK robot day](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=9b28315bab82ad10456fca08b386d83d).

=summer term 2010=
    **Computer science lecturer**, *Osnabrück University*, Germany.  
    Hands-on training [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=dd6229ef56b1ffdf56bc3cea1f6d0bc7).
=winter term 2008/09=
    **Computer science tutor**, *Osnabrück University*, Germany.  
    Tutor and exercises for [robotics](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=c8530dde4ade148608a421aa7649fe9f).
=summer term 2008=
    **Computer science tutor**, *Osnabrück University*, Germany.  
    Tutor for [3D image processing](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=df85be3c287358c383ebeca770fa0616).
=summer term 2007=
    **Computer science tutor**, *Osnabrück University*, Germany.  
    Tutor for [3D image processing for mobile robots](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=cdb9b04d27c0d1990e099899059b187f).
=winter term 2006/07=
    **Applied system science tutor**, *Osnabrück University*, Germany.  
    Tutor and exercises for [numerical solutions for ordinary and partial differential equation](https://www.usf.uos.de/institut/veranstaltungen/lehrveranstaltungen.html?module=Lecturedetails&target=14954&source=14954&range_id=49ce7409e6d0339a42b2f3626cd72647&seminar_id=618abba6f3f36c39247309290905e6c2).
=winter term 2006/07=
    **Computer science tutor**, *Osnabrück University*, Germany.  
    Tutor for [Introduction to C/C++](https://web.archive.org/https://www.informatik.uni-osnabrueck.de/studium/veranstaltungen/aktuelle_veranstaltungen.html?module=Lecturedetailsalle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955target=14953alle_veranstaltungen.html?module=Lecturedetails&target=14955&source=14955source=14953&range_id=0c3ad80568c7b478c148af31ae56baa9&seminar_id=74e8e508814d91a97d5d02af7e8848bc).
=summer term 2005=
    **Applied system science tutor**, *Osnabrück University*, Germany.  
    Tutor for [numerical solutions for partial differential equation](https://www.usf.uos.de/institut/veranstaltungen/lehrveranstaltungen.html?module=Lecturedetails&target=14954&source=14954&range_id=49ce7409e6d0339a42b2f3626cd72647&seminar_id=96e46f9054a069fab9ca5b5c790ebe64).
=winter term 2004/05=
    **Computer science tutor**, *Osnabrück University*, Germany.  
    Tutor and exercises for [computer science C](https://web.archive.org/http://www.vorlesungen.uos.de/informatik/c04/) (advanced programming, object orientation in Java).


# Conferences and Talks

=December 2024=
    **38C3**, *Hamburg*, Germany.  
    [38st Chaos Communication Congress (38C3)](https://events.ccc.de/congress/2024/)
=November 2024=
    **MiniDebConf Toulouse 2024**, *Toulouse*, France.  
    [MiniDebConf Toulouse](https://toulouse2024.mini.debconf.org/).
=March 2024=
    **IK 2024**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/), focus theme: "Resilience, Robustness, Responsibility".
=December 2023=
    **37C3**, *Hamburg*, Germany.  
    [37st Chaos Communication Congress (37C3)](https://events.ccc.de/congress/2023/)
=August 2023=
    **CCCamp 2023**, *Ziegeleipark Mildenberg*, Germany.  
    [7th Chaos Communication Camp (CCCamp2023)](https://events.ccc.de/camp/2023/)
=May 2023=
    **Debian Reunion Hamburg 2023**, *Hamburg*, Germany.  
    [Debian Reunion Hamburg](https://wiki.debian.org/DebianEvents/de/2023/DebianReunionHamburg).
=March 2023=
    **IK 2023**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/), focus theme: "Dynamics of Experience".
=May 2022=
    **Debian Reunion Hamburg 2022**, *Hamburg*, Germany.  
    [Debian Reunion Hamburg](https://wiki.debian.org/DebianEvents/de/2022/DebianReunionHamburg).
=March 2022=
    **VIK 2022**, *Gather Town*, Internet.  
    [Interdisciplinary College](https://interdisciplinary-college.org/), focus theme: "Flexibility".
=October 2021=
    **ROS World 2021**, *swapcard*, Internet.  
    [ROS World developers conference](https://roscon.ros.org/2021/)
=March 2021=
    **VIK 2021**, *Gather Town*, Internet.  
    [Interdisciplinary College](https://interdisciplinary-college.org/), focus theme: "Connected in Cyberspace".
=December 2020=
    **rC3 2020**, *WorkAdventure*, Internet.  
    [rC3 – remote Chaos Experience](https://events.ccc.de/2020/09/04/rc3-remote-chaos-experience/)
=November 2020=
    **ROS World 2020**, *swapcard*, Internet.  
    [ROS World developers conference](https://roscon.ros.org/2020/)
=December 2019=
    **36C3**, *Leipzig*, Germany.  
    [36st Chaos Communication Congress (36C3)](https://events.ccc.de/congress/2019/)
=August 2019=
    **CCCamp 2019**, *Ziegeleipark Mildenberg*, Germany.  
    [6th Chaos Communication Camp (CCCamp2019)](https://events.ccc.de/camp/2019/)
=March 2019=
    **IK 2019**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/),
focus theme: "Out of your Senses".
Lecture series "Insights into your Robot - Seeing the World from their Perspective"

=December 2018=
    **35C3**, *Leipzig*, Germany.  
    [35st Chaos Communication Congress (35C3)](https://events.ccc.de/congress/2018/)
=November 2018=
    **FOSS Compliance Seminar**, *Munich*, Germany.  
    Software Compliance Academy
=May 2018=
    **MiniDebConf Hamburg 2018**, *Hamburg*, Germany.  
    [Mini DebConf Hamburg](https://wiki.debian.org/DebianEvents/de/2018/MiniDebConfHamburg).
=March 2018=
    **IK 2018**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/), focus theme: "ME, MY SELF, AND I".
=December 2017=
    **34C3**, *Leipzig*, Germany.  
    [34st Chaos Communication Congress (34C3)](https://events.ccc.de/congress/2017/)
=March 2017=
    **IK 2017**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/), focus theme: "Creativity and Intelligence in Brains and Machines".
=December 2016=
    **33C3**, *Hamburg*, Germany.  
    [33st Chaos Communication Congress (33C3)](https://events.ccc.de/congress/2016/)
=March 2016=
    **IK 2016**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/),
focus theme: "Transitions and Transformations in Cognition, Biology, and Interactive Systems".
lecture series "Hands-On Mobile Robotics"

=December 2015=
    **32C3**, *Hamburg*, Germany.  
    [32st Chaos Communication Congress (32C3)](https://events.ccc.de/congress/2015/)
=October 2015=
    **ROSCon 2015**, *Hamburg*, Germany.  
    [ROSCon developers conference](https://roscon.ros.org/2015/)
=September 2015=
    **UAV tutorial**, *Hannover*, Germany.  
    Handling of a commercial used multi copter (UAV) in theory and praxis, given by André Scholz
=August 2015=
    **AMAV 2015**, *Birlinghoven Castle*, Germany.  
    [TRADR Summer School on Autonomous Micro Aerial Vehicles](https://web.archive.org/web/20160310054808/https://www.iais.fraunhofer.de/index.php?id=6257)
=August 2015=
    **FrOSCon 2015**, *Bonn-Rhein-Sieg*, Germany.  
    [Free and Open Source Software Conference](https://www.froscon.de/)
=March 2015=
    **IK 2015**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/), focus theme: "From Neuron to Person: Assembling Behavior and Cognition"
=December 2014=
    **31C3**, *Hamburg*, Germany.  
    [31st Chaos Communication Congress (31C3)](https://events.ccc.de/congress/2014/)
=March 2014=
    **Fraunhofer IPA**, *Stuttgart*, Germany.  
    Talk about PCL at the [3D environmental perception](https://web.archive.org/web/20160910062732/https://www.stuttgarter-produktionsakademie.de/3D-Umgebungserfassung.698.0.html) workshop
=March 2014=
    **IK 2014**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/), focus theme: "Cognition 3.0 - the social mind in the connected world"
=December 2013=
    **30C3**, *Hamburg*, Germany.  
    [30th Chaos Communication Congress (30C3)](https://events.ccc.de/congress/2013/)
=December 2013=
    **Örebro University**, *Örebro*, Sweden.  
    First Örebro [Winter School](http://aass.oru.se/Agora/Lucia2013/) on "Artificial Intelligence and Robotics"
=November 2013=
    **Bielefeld University**, *Bielefeld*, Germany.  
    Talk on [PCL and Robotics in Osnabrück](https://www.cit-ec.de/node/9683) at the CoR-Lab
=May 2013=
    **ROSCon 2013**, *Stuttgart*, Germany.  
    [ROSCon developers conference](https://roscon.ros.org/2013/)
=May 2013=
    **ICRA 2013**, *Karlsruhe*, Germany.  
    Talk on development for PCL as well as registration and mapping with the PCL at the [PCL tutorial](https://web.archive.org/http://pointclouds.org/media/icra2013.html)
=March 2013=
    **IK 2013**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/), focus theme: "Wicked Problems, Complexity and Wisdom"
=May 2012=
    **ROSCon 2012**, *St. Paul*, USA.  
    [ROSCon developers conference](https://roscon.ros.org/2012/)
=May 2012=
    **ICRA 2012**, *St. Paul*, USA.  
    Talk on registration and mapping with the PCL at the [PCL tutorial](https://web.archive.org/http://pointclouds.org/media/icra2012.html)
=March 2012=
    **IK 2012**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/),
focus theme: "Emotion and Aesthetics".
Gave a session on "Emotion and Aesthetics in Computer Interfaces"

=November 2011=
    **ICCV 2011**, *Barcelona*, Spain.  
    Talk on registration and mapping with the PCL in the [PCL tutorial](https://web.archive.org/http://pointclouds.org/media/iccv2011.html)
=June 2011=
    **RSS 2011**, *Los Angeles*, USA.  
    Talk on registration and mapping with the PCL in the [PCL tutorial](https://web.archive.org/http://pointclouds.org/media/rss2011.html)
=March 2011=
    **IK 2011**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/),
focus theme: "Autonomy, Decisions & Free Will".
Gave a session on mobile robots and SLAM

=February 2011=
    **10th Oldenburger 3D days**, *Jade University of Applied Sciences*, Germany.  
    Optical 3D measuring, photogrammetry, laser scanning
=October 2010=
    **KogWis 2010**, *University of Potsdam*, Germany.  
    10th Biannual Meeting of the German Society for Cognitive Science,,
=July 2010=
    **Co3AUV**, *Jacobs University Bremen*, Germany.  
    summer school on cooperative cognitive control for autonomous underwater vehicles
=March 2010=
    **IK 2010**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/), focus theme: "Play, Act and Learn"
=September 2009=
    **ECMR 2009**, *Mlini/Dubrovnik*, Croatia.  
    European Conference on Mobile Robots,
=March 2008=
    **IK 2008**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/), focus theme: "Cooperation"
=September 2007=
    **KI 2007**, *Osnabrück University*, Germany.  
    30th German Conference on Artificial Intelligence (member of the organization team)
=March 2007=
    **IK 2007**, *Günne at Lake Möhne*, Germany.  
    [Interdisciplinary College](https://interdisciplinary-college.org/previous-iks/), focus theme: "Embodied Minds"
=December 2006=
    **23C3**, *Berlin*, Germany.  
    [23rd Chaos Communication Congress (23C3)](https://events.ccc.de/congress/2006/)
=November 1999=
    **Linux day**, *Bremen*, Germany.  
    [2. Bremer LinuxTag](https://www.debian.org/events/1999/1128-linuxtag)


# Academic self-administration

=2005 - 2007=
    **Member of the [faculty council](https://www.mathinf.uni-osnabrueck.de)**, *Osnabrück University*, Germany.
=2004 - 2006=
    **Student representative ([Fachschaft](https://fachschaft.mathinf.uni-osnabrueck.de))**, *Osnabrück University*, Germany.
