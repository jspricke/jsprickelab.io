# Personal Organizer

- [Remind](https://dianne.skoll.ca/projects/remind/)
- [wyrd](https://gitlab.com/wyrd-calendar/wyrd/)
- [taskwarrior](https://taskwarrior.org/)
- [vit](https://github.com/vit-project/vit)
- [abook](http://abook.sourceforge.net)

# File System

- [cryptsetup](https://gitlab.com/cryptsetup/cryptsetup)
- [pam-mount](http://pam-mount.sourceforge.net/)
- [unison](https://www.cis.upenn.edu/~bcpierce/unison/)
- Alternatives: [9P](https://9p.cat-v.org/)

# Browser

- [surf](https://surf.suckless.org) (Patches: a lot, ask me).
- Alternatives: [uzbl](https://www.uzbl.org/), [dwb](https://portix.bitbucket.org/dwb/), [surfraw](https://packages.debian.org/sid/web/surfraw)

# Music Manager

- [Quod Libet](https://quodlibet.readthedocs.io/)
- [picard](https://picard.musicbrainz.org/)

# Development

- [gcc](https://gcc.gnu.org/)
- [ccache](https://ccache.samba.org/)
- [strace](https://sourceforge.net/projects/strace/)
- [gdb](https://www.gnu.org/software/gdb/)
- [valgrind](http://www.valgrind.org/)
- Alternatives: [llvm](https://www.llvm.org/)

# Small Scripts

sfs: sshfs wrapper

~~~
#!/bin/sh -e

args=""
if [ $# -gt 1 ]; then
	args="$1"
	shift
fi

host=$(echo "$1" | cut -d : -f 1)

if [ -d "$host" ]; then
	fusermount -uz "$host"
	rmdir "$host"
else
	mkdir "$host"
	case "$1" in
	*:*)
		sshfs $args "$1" "$host" || rmdir "$host"
		;;
	*)
		sshfs $args "$1": "$host" || rmdir "$host"
		;;
	esac
fi
~~~

pp: (un)pause media players:

~~~
#!/bin/sh
for i in "/run/user/$(id -u)/mpv_socket_"*; do
	[ -S "$i" ] && echo '{ "command": ["set", "pause", "yes"] }' | nc -w1 -U "$i" >/dev/null &
done
quodlibet --play-pause 2>/dev/null
~~~

dwm-status, dmenu_win, kbled, m, n, o, scr, sfs, tv, umount, urldump (unpublished, write me if you are interested).

# Other Software

- Login manager: xdm
- Banking:
  - [Hibiscus](https://www.willuhn.de/products/hibiscus/)
  - Alternatives: [aqbaning](https://www.aquamaniac.de/sites/aqbanking/)
- Webserver:
  - [nginx](https://nginx.org/)
  - Alternatives: [gatling](https://www.fefe.de/gatling)
- Media player:
  - [mpv](https://mpv.io)
  - [ffmpeg](https://ffmpeg.org)
  - Alternatives: [vlc](https://www.videolan.org/vlc/)
- Image browser: [nsxiv](https://github.com/nsxiv/nsxiv)
- Package management:
  - [aptitude](https://wiki.debian.org/Aptitude)
  - [apt-listchanges](https://packages.debian.org/wheezy/apt-listchanges)
  - [apt-listbugs](https://packages.debian.org/sid/apt-listbugs)
  - Alternatives: [apt](https://wiki.debian.org/Apt)
- CD ripper: [abcde](https://abcde.einval.com/wiki/)
- Compression manager: [atool](https://www.nongnu.org/atool/)
- Download helpers:
  - [wget](https://www.gnu.org/software/wget/)
  - [yt-dlp](https://github.com/yt-dlp/yt-dlp)
  - Alternatives: [curl](https://curl.haxx.se/)
- PDF viewer:
  - [zathura](https://pwmt.org/projects/zathura/)
  - [pdfgrep](https://sourceforge.net/projects/pdfgrep/)
  - [poppler-utils](https://poppler.freedesktop.org/)
  - Alternatives [evince](https://wiki.gnome.org/Apps/Evince)
- Graph:
  - [gnuplot](http://gnuplot.info)
  - Alternatives: [python-matplotlib](https://matplotlib.org)
- GPS:
  - [gpsbabel](https://www.gpsbabel.org)
  - [GPX Viewer](https://github.com/andrewgee/gpxviewer)
  - [mytourbook](http://mytourbook.sourceforge.net)
- Presentation:
  - [latex-beamer](https://github.com/josephwright/beamer/)
  - [impressive](http://impressive.sourceforge.net)
  - Alternatives: [pdf-presenter-console](https://pdfpc.github.io)
- Python: [ipython](https://ipython.org)
- Office:
  - [LaTeX](https://www.latex-project.org)
  - [LibreOffice](https://www.libreoffice.org/)
- Network:
  - [mtr](https://www.bitwizard.nl/mtr/)
  - [netcat](https://packages.debian.org/unstable/netcat-openbsd)
  - [mmap](https://nmap.org/)
- Spreadsheet: [sc](https://packages.debian.org/sid/sc)
- Planetarium: [stellarium](https://stellarium.org/)
- Mouse: [unclutter-xfixes](https://github.com/Airblader/unclutter-xfixes)
