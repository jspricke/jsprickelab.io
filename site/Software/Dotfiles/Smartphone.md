# Operating System

- [CalyxOS](https://calyxos.org)
  - Alternativ: [LineageOs](https://lineageos.org)
- [F-Droid](https://f-droid.org)

# Apps

- [Audio Spectrum Analyzer](https://f-droid.org/packages/org.woheller69.audio_analyzer_for_android/)
- [Binary Eye](https://f-droid.org/packages/de.markusfisch.android.binaryeye/)
- [Bubble](https://f-droid.org/packages/org.woheller69.level/)
- [Cirrus](https://f-droid.org/packages/org.woheller69.omweather/)
- [DAVx⁵](https://f-droid.org/packages/at.bitfire.davdroid/)
- [Equate](https://f-droid.org/packages/com.llamacorp.equate/)
- [Etar - OpenSource Calendar](https://f-droid.org/packages/ws.xsoh.etar/)
- [F-Droid Basic](https://f-droid.org/packages/org.fdroid.basic/)
- [Fennec F-Droid](https://f-droid.org/packages/org.mozilla.fennec_fdroid/)
- [GPSTest](https://f-droid.org/packages/com.android.gpstest.osmdroid/)
- [Hacker's Keyboard](https://f-droid.org/packages/org.pocketworkstation.pckeyboard/)
- [Irregular Expressions](https://f-droid.org/packages/mf.asciitext.lite/)
- [Jitsi Meet](https://f-droid.org/packages/org.jitsi.meet/)
- [K-9 Mail](https://f-droid.org/packages/com.fsck.k9/)
- [M.A.L.P.](https://f-droid.org/packages/org.gateshipone.malp/)
- [Mather](https://f-droid.org/packages/org.icasdri.mather/)
- [Metro - A music player for Android](https://f-droid.org/packages/io.github.muntashirakon.Music/)
- [MuPDF viewer](https://f-droid.org/packages/com.artifex.mupdf.viewer.app/)
- [NewPipe](https://f-droid.org/packages/org.schabi.newpipe/)
- [NFSee - NFC Card Helper](https://f-droid.org/packages/im.nfc.nfsee/)
- [Offi](https://f-droid.org/packages/de.schildbach.oeffi/)
- [Open Camera](https://f-droid.org/packages/net.sourceforge.opencamera/)
- [OpenFoodFacts](https://f-droid.org/packages/openfoodfacts.github.scrachx.openfood/)
- [OpenTasks](https://f-droid.org/packages/org.dmfs.tasks/)
- [Organic Maps: Hike, Bike, Drive Offline](https://f-droid.org/packages/app.organicmaps/)
- [PassAndroid](https://f-droid.org/packages/org.ligi.passandroid/)
- [SatStat](https://f-droid.org/packages/com.vonglasow.michael.satstat/)
- [Share via HTTP](https://f-droid.org/packages/com.MarcosDiez.shareviahttp/)
- [Sky Map](https://f-droid.org/packages/com.google.android.stardroid/)
- [SMS Backup+](https://f-droid.org/packages/com.zegoggles.smssync/)
- [Termux](https://f-droid.org/packages/com.termux/)
- [Todo Agenda](https://f-droid.org/packages/org.andstatus.todoagenda/)
- [Weechat-Android](https://f-droid.org/packages/com.ubergeek42.WeechatAndroid/)
- [WhereYouGo](https://f-droid.org/packages/menion.android.whereyougo/)
- [whoBIRD](https://f-droid.org/packages/org.woheller69.whobird/)
- [WiFiAnalyzer](https://f-droid.org/packages/com.vrem.wifianalyzer/)
- [Zapp](https://f-droid.org/packages/de.christinecoenen.code.zapp/)
- [c:geo](https://fdroid.cgeo.org)

# Remote Connections and Synchronization

- [OpenSSH](https://www.openssh.com/)
- [Dovecot](https://www.dovecot.org/)
- [Weechat](https://weechat.org/)
- [Radicale-Remind](https://github.com/jspricke/radicale-remind)
- [Mosh](https://mosh.org/)
