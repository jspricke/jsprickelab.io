# Software

- [Bitlbee](https://bitlbee.org)
- [Weechat](https://weechat.org)
- [Mutt](https://www.mutt.org)
- [Fetchmail](http://www.fetchmail.info)
- [Procmail](http://www.procmail.org)
- [msmtp](http://msmtp.sourceforge.net/)
- [esmtp](http://esmtp.sourceforge.net/)
- [spamassassin](https://spamassassin.apache.org/)
- [lbdb](https://www.spinnaker.de/lbdb/)
- Alternatives: [minbif](https://symlink.me/projects/minbif/wiki), [notmuch](https://notmuchmail.org/), [WanderLust](https://www.emacswiki.org/emacs/WanderLust)

# Mutt Config

~~~
set sendmail="/usr/bin/msmtp"
set use_from=yes
set realname="Jochen Sprickerhof"
set from="my@mail.example"
set use_envelope_from=yes
set mbox_type=Maildir
set folder="~/.maildir"
set mbox="+Posteingang"
set postponed="+Entwurf"
set spoolfile="+Posteingang"
set record="+Posteingang"
mailboxes +Posteingang +Spam

alternates ((my|other)@mail.example)

lists debian-(news|announce|security)@lists.debian.org

subscribe dev@suckless.org

source ~/.config/mutt/abook-export|

send-hook . "set editor=\"nvim -c 'set tw=72' +/^$ -c 'set fo+=w' -c noh %s\""
send-hook '~C "\.(de|at)$"' "set editor=\"nvim -c 'set tw=72' +/^$ -c 'set spelllang=de' -c 'set fo+=w' -c noh %s\""
set editor="nvim -c 'set tw=72' +/^$ -c 'set fo+=w' -c noh %s"

alternative_order text/calendar text/plain text/html
auto_view text/calendar text/html

set abort_nosubject=no
set abort_unmodified=no
set attribution="* %n <%a> [%{%Y-%m-%d %H:%M}]:"
set auto_tag=yes
set collapse_unread=no
set confirmappend=no
set crypt_opportunistic_encrypt=yes
set crypt_protected_headers_save=yes
set date_format="%d.%m.%Y %H:%M"
set edit_headers=yes
set fast_reply=yes
set folder_format="%4C %-20.20f %N %d"
set index_format="%4C %Z %2M %1H %D %-15.15F %s %*"
set mail_check_stats=yes
set markers=no
set mask='!^(\.[^.m]|\.m[^a]|\.mail[^d]|dovecot-acl-list)'
set menu_move_off=no
set pager_index_lines=5
set pgp_autosign=yes
set pgp_auto_decode=yes
set pgp_use_gpg_agent=yes
set postpone=ask-no
set print_command="muttprint"
set query_command="lbdbq %s"
set recall=ask-no
set reverse_alias=yes
set send_charset="us-ascii:utf-8"
set sidebar_format="%B%* %?F?[%F] ?%?N?(%N) ?%S"
set sidebar_visible=yes
set sidebar_width=25
set sort_aux=last-date-received
set sort=threads
set text_flowed
set tilde
set wait_key=no

bind browser,pager,attach h exit
bind browser,pager,attach <left> exit
macro index h "<change-folder>?<toggle-mailboxes>1<return>" "show incoming mailboxes list"
macro index <left> "<change-folder>?<toggle-mailboxes>1<return>" "show incoming mailboxes list"

bind pager j next-line
bind pager <down> next-line
bind pager k previous-line
bind pager <up> previous-line

bind generic,browser l select-entry
bind generic,browser <right> select-entry
bind index l display-message
bind index <right> display-message
bind pager l view-attachments
bind pager <right> view-attachments
bind attach,compose l view-attach
bind attach,compose <right> view-attach

bind index <Esc>l limit
bind index ' ' next-unread
bind browser x check-new
bind index x sync-mailbox
bind pager,attach H display-toggle-weed
bind index B bounce-message

bind index - collapse-thread
bind index _ collapse-all

macro index y "<change-folder>?"
macro index \Cb "<search>~b"
macro pager A "<pipe-entry>abook --add-email<enter>"
macro pager T "<pipe-entry>~/.config/mutt/mailhops|less -c<enter>"
macro attach R "<pipe-entry>ics2rem | ~/.config/mutt/rem-filter >> ~/.reminders<enter>"
macro pager R "<view-attachments><down><down><pipe-entry>ics2rem | ~/.config/mutt/rem-filter >> ~/.reminders<enter><exit>" "process Exchange calndar invitations"
macro index,pager S "<pipe-entry>/usr/bin/sa-learn --spam\n<delete-entry>"
macro index,pager X "<pipe-entry>/usr/bin/sa-learn --ham\n<next-unread>"
macro index,pager \CP <sidebar-prev><sidebar-open>
macro index,pager \CN <sidebar-next><sidebar-open>
macro index,pager b '<enter-command>toggle sidebar_visible<enter><redraw-screen>'
macro index,pager F '<enter-command>unset resolve<enter><clear-flag>N<enter-command>set resolve<enter><flag-message>'

macro index,pager <Esc>1 <change-folder>+Posteingang<enter>
macro index,pager <Esc>2 <change-folder>+Spam<enter>

ignore *
unignore from: date subject to cc reply-to:
unignore organization organisation
unignore user-agent: x-agent: x-mailer: x-newsreader:
unignore newsgroups: posted-to:

unhdr_order *
hdr_order Date: To: Cc: X-Newsreader: X-Mailer: Organization: Organisation: User-Agent: Newsgroups: Reply-To: From: Subject:

color attachment white magenta
color body cyan default "ftp://[^ ]*"
color body brightgreen default "[[:alnum:]][-+.#_[:alnum:]]*@[-+.[:alnum:]]*[[:alnum:]]"
color body cyan default "<URL:[^ ]*>"
color bold green default
color error brightred default

color header brightyellow default "^cc:"
color header green default "^date:"
color header brightyellow default "^from"
color header brightcyan default "^from:"
color header brightyellow default "^newsgroups:"
color header brightyellow default "^reply-to:"
color header brightcyan default "^subject:"
color header green default "^to:"
color header brightyellow default "^x-mailer:"
color header brightyellow default "^message-id:"
color header brightyellow default "^Organization:"
color header brightyellow default "^Organisation:"
color header brightyellow default "^User-Agent:"
color header brightyellow default "^message-id: .*pine"

color indicator white blue
color markers brightred default
color message white blue

color normal default default # pager body

color quoted cyan default
color quoted1 brightyellow default
color quoted2 brightred default
color quoted3 green default
color quoted4 cyan default
color quoted5 brightyellow default
color quoted6 brightred default
color quoted7 green default

color signature brightred default
color status white blue
color tilde blue default
color tree brightmagenta default
color underline brightyellow default
color body brightyellow default "[;:]-[)/(|]" # colorise smileys
color body brightblue default "(https?|ftp|news|telnet|finger)://[^ ]*"

color index brightyellow default ~N # New
color index brightyellow default ~O # Old
color index brightgreen default '~s jochensp'
color index brightgreen default '~p' # mail to myself
color index brightcyan default '~P' # mail from myself
color index brightyellow default ~F # Flagged
color index blue default ~T # Tagged
color index red default ~D # Deleted
~~~
