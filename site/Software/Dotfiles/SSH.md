# Software

- [OpenSSH](https://www.openssh.com/)
- [sshfs](https://github.com/libfuse/sshfs)
- [tsocks](http://tsocks.sourceforge.net/)
- Alternative: [OpenVPN](https://openvpn.net/)

# SSH Config

~~~
ServerAliveInterval 30
ControlMaster auto
ControlPath ${XDG_RUNTIME_DIR}/ssh-%r@%h:%p
ControlPersist 4

Host host
Hostname hostname
User user
IdentityFile ~/.ssh/id_rsa_host
ForwardX11 yes
DynamicForward 8083
RemoteForward 2233 127.0.0.1:22

Host host2
ProxyJump host
~~~
