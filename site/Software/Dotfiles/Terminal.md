# Software

- [st](https://st.suckless.org) (Patches: [scrollback](https://st.suckless.org/patches/scrollback), alert, urlopen)
- Alternatives: [rxvt-unicode](https://software.schmorp.de/pkg/rxvt-unicode.html)

# Terminal Multiplexer

- [tmux](https://tmux.github.io/)
- Alternatives: [screen](https://www.gnu.org/software/screen/), [dvtm](http://www.brain-dump.org/projects/dvtm/)

# tmux Config

~~~
set -g base-index 1
set -g default-command "/usr/bin/fish"
set -g set-titles on
set -g set-titles-string "tmux.#I.#W"
set -g status-left-style bg=red,fg=white
set -g status-left "@#H "
set -g status-right "%a %d.%m. %H:%M"
set -g status-right-style bg=blue,fg=white
set -g escape-time 0
set -g aggressive-resize on
set -g automatic-rename off
set -g window-status-current-style bg=red,fg=white
set -g focus-events on

unbind C-b
set -g prefix C-a
bind a send-prefix

bind C-s choose-session
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R
bind s split-window -v
bind v split-window -h
bind C-a last-window
~~~
