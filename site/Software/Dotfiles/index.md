# Overall Setup

I have been asked a number of times about my computer setup, so this page describes some of the details.
If you have any questions or proposals on how to improve it, please don't hesitate to write.

General considerations: I want my computer to react fast.
I don't want to wait while there is some stupid animation going on or while a broken program is loading.
I will rather learn a keyboard shortcut instead of pushing the mouse around.
Paradigm: I am the top scheduler, programs should start in the background and signal when they need input.

I have a smartphone (Android), a laptop (13") and a server (Odroid XU4).
The smartphone is with me all the time and often a laptop as well.
The server is behind a dialup connection and serves as a backup as well.
All of my `$HOME` is synced between the laptop and the server.
Also I like to have my `$HOME` clean, with no cruft lying around (especially no `~/Desktop`).

# Operating System

[Debian](https://debian.org) GNU/Linux unstable.
Pro: reasonable default setup, huge collection of software, package management.
I'm maintaining all my configurations and software choices in a Debian package.
This makes it easy to deploy and install software stacks for a specific task.

Alternatives:

- [stali](http://sta.li/)
- [*BSD](https://en.wikipedia.org/wiki/Berkeley_Software_Distribution)
- [Hurd](https://www.gnu.org/software/hurd/)
- [Plan 9](https://9p.io/plan9/)

# Quote, don't Copy

This is meant as a reference and inspiration, not as a fixed setup.
It took quite some time to gather this, but it gave me the time to get used to it, as well.
Take what you find interesting, integrate it into your setup bit by bit and watch yourself to see if it fits your habits.
Just copying the complete setup will most probably result in the opposite of what you expected, i.e., a drop in productivity.
