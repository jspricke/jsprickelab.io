# Software

- [Git](https://git-scm.com/)

# Git Config

~~~
[user]
	name = Name
	email = Mail
[alias]
	ci = commit
	st = status --show-stash
	co = checkout
	ft = fetch --tags
	wdiff = diff --color-words --no-prefix
	graph = log --graph --decorate --branches --remotes
	check = !git status && git log @{u}.. && git branch && git stash list
	lg = log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset' --abbrev-commit --date=format:'%a %d.%m.%Y %H:%M' --no-prefix
	rlg = log --color --walk-reflogs --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %gs %Cgreen(%gD) %C(bold blue)<%an>%Creset' --abbrev-commit --date=format:'%a %d.%m.%Y %H:%M' --no-prefix
	pushf = push --force-with-lease
	stash-all = stash save --include-untracked
	new = log -p --no-prefix --reverse ORIG_HEAD..
	df = diff --no-prefix
	mr = push -u -o merge_request.create
	cir = commit -eF .git/COMMIT_EDITMSG
[merge]
	tool = vimdiff
	conflictstyle = zdiff3
[mergetool]
	keepBackup = false
[merge "dpkg-mergechangelogs"]
	name = debian/changelog merge driver
	driver = dpkg-mergechangelogs -m %O %A %B %A
[rebase]
	autosquash = true
	stat = true
	updateRefs = true
[sendemail]
	smtpserver = "/usr/bin/msmtp"
	confirm = auto
[pull]
	rebase = false
[tag]
	sort = version:refname
[url "git@github.com:"]
	pushInsteadOf = "https://github.com/"
[url "git@gitlab.com:"]
	pushInsteadOf = "https://gitlab.com/"
[url "git@salsa.debian.org:"]
	pushInsteadOf = "https://salsa.debian.org/"
[url "git@codeberg.org:"]
	pushInsteadOf = "https://codeberg.org/"
~~~
