# Software

- [Fish](https://fishshell.com)
- Alternatives: [zsh](https://www.zsh.org), [bash](https://www.gnu.org/software/bash/)

# ~/.profile

~~~
[ "$BROWSER" ] && exit
export COLORFGBG=";0"
export BROWSER=surf
export EDITOR=nvim
export CFLAGS="-Wall -Wextra -Wpedantic -Wconversion -pipe -fdiagnostics-color=auto -O3 -march=native"
export CXXFLAGS="$CFLAGS -Weffc++ -Wold-style-cast"
export LDFLAGS="-s -Wl,-z,defs"
export BTS_SENDMAIL_COMMAND="msmtp -t"
export _JAVA_AWT_WM_NONREPARENTING=1
export LC_MESSAGES=C.UTF-8
# mkdir -p ~/.cache/python; ln -s -t ~/.cache/python /usr
export PYTHONPYCACHEPREFIX="$HOME/.cache/python"
export CCACHE_DIR="$HOME/.cache/ccache"
export DEBUGINFOD_URLS="https://debuginfod.debian.net"
export LESS=-FiRw
export PYTHONBREAKPOINT=ipdb.set_trace
test -d "$HOME/.local/bin" && export PATH="$HOME/.local/bin:$PATH"
test -d "$HOME/.local" && export CMAKE_PREFIX_PATH="$HOME/.local:$CMAKE_PREFIX_PATH"
test -d "$HOME/.local/include" && export CPATH="$HOE/.local/include:$CPATH"
test -d "$HOME/.local/lib" && export LD_LIBRARY_PATH="$HOME/.local/lib:$LD_LIBRARY_PATH"
test -d "$HOME/.local/lib" && export LIBRARY_PATH="$HOME/.local/lib:$LIBRARY_PATH"
test -d "$HOME/.local/lib/pkgconfig" && export PKG_CONFIG_PATH="$HOME/.local/lib/pkgconfig:$PKG_CONFIG_PATH"
test -d "$HOME/.local/man" && export MANPATH="$HOME/.local/man:$MANPATH"
test -d "$HOME/.local/share/texmf" && export TEXMFHOME="$HOME/.local/share/texmf"
test -d /usr/lib/ccache && export PATH="/usr/lib/ccache:$PATH"
~~~

# Fish Config

~~~
source "$HOME/.profile"
set -x MAKEFLAGS -j(nproc)
set -x GPG_TTY (tty)

if not status --is-interactive
  exit
end

set -g fish_greeting
set -q SSH_CONNECTION; and set __fish_prompt_hostname (set_color brblue)(prompt_hostname)' '
set __fish_prompt_prompt (set_color green -b normal)
set __fish_prompt_normal (set_color normal -b normal)
set __fish_prompt_jobs (set_color -b green)
set __fish_prompt_status (set_color -b red)
set __fish_prompt_envdirs (set_color cyan)
set fish_color_command (set_color white)

function fish_prompt
  set -l last_status $status
  set -l jobs (jobs | wc -l)
  echo -n -s -e "\e[ m"
  test $last_status -ne 0; and echo -n -s "$__fish_prompt_status" "$last_status" "$__fish_prompt_normal" ' '
  test $jobs -ne 0; and echo -n -s "$__fish_prompt_jobs" "$jobs" "$__fish_prompt_normal" ' '
  test -n "$envdirs"; and echo -n -s "$__fish_prompt_envdirs" "$envdirs" ' '
  echo -n -s "$__fish_prompt_user" "$__fish_prompt_hostname" "$__fish_prompt_prompt" (prompt_pwd) '> ' "$__fish_prompt_normal"
end

function postexec_bell --on-event fish_postexec; tput bel; end

function multicd; echo cd (string repeat -n (math (string length -- $argv[1]) - 1) ../); end
abbr --add dotdot --regex '^\.\.+$' --function multicd

function last_history_items; echo (string join '; ' $history[(string sub -s 2 $argv[1])..1]); end
abbr --add hist --position anywhere --regex '^!\d+$' --function last_history_items

bind \ej up-or-search
bind \ek down-or-search
bind \e, history-token-search-forward
bind \en nextd-or-forward-word
bind \ep prevd-or-backward-word
bind \eP __fish_paginate
bind \cw backward-kill-word
bind \cp history-search-backward
bind \cn history-search-forward
bind \cg self-insert expand-abbr

function pkg-config
  /usr/bin/pkg-config $argv | tr -s ' ' '\n'
end

set -x LS_COLORS (dircolors -c | string split "'")[2]

alias ls '/bin/ls --color=auto'
alias l 'ls'
alias ll '/bin/ls -lh --color=auto'
alias a 'ls -A'
alias la 'll -A'
alias ip 'ip --color=auto'
alias diff='diff --color=auto'
alias grep 'grep --color=auto'
alias psl 'ps auxf | less'
alias psg 'pgrep -af'
alias hist 'history | less'
alias hig 'history --search'
alias df 'df -h'
alias free 'free -h'
alias rsync 'rsync -aSHAXhP'
alias serv 'python3 -m http.server'
alias ssh_insec 'ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -S none'
alias scp_insec 'scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -S none'
alias pling 'tput bel'
alias emacs 'emacs -nw'
alias x 'atool -qx'
alias cmake 'cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME/.local -DCMAKE_EXPORT_COMPILE_COMMANDS=ON'
alias cmaked 'cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=$HOME/.local -DCMAKE_EXPORT_COMPILE_COMMANDS=ON'
alias p 'ipython3 --no-banner --no-confirm-exit --colors=Linux --TerminalInteractiveShell.true_color=True'
alias rviz 'rviz -s ""'
alias soffice 'soffice --nologo'
alias qr 'qrencode -t ansi'
alias ti 'chafa -c full --font-ratio 8/14'
~~~
