# Window Manager

- [dwm](https://dwm.suckless.org)
- Patches:
  - [fancybar](https://dwm.suckless.org/patches/fancybar)
  - attachbelow
  - [warp](https://dwm.suckless.org/patches/warp)
  - [pertag](https://dwm.suckless.org/patches/pertag)
  - [hide_vacant_tags](https://dwm.suckless.org/patches/hide_vacant_tags)
- [lsw](https://tools.suckless.org/x/lsw)
- [dmenu](https://tools.suckless.org/dmenu/)
- Alternatives: Awesome, [Xmonad](https://xmonad.org/), [I3](https://i3wm.org/)

# ~/.xsession

~~~
#!/bin/sh

. "$HOME/.profile"

trap "RUN=false" TERM

: > "$HOME/.xsession-errors"

xset -b
xsetroot -solid black
setxkbmap de nodeadkeys caps:escape compose:rctrl
st &
unclutter --timeout 1 & UNCLUTTER=$!
if [ "$(date +"%H")" -ge 22 ] || [ "$(date +"%H")" -lt 5 ]; then
  brightness 500
elif [ "$(date +"%H%M")" -ge 2030 ]; then
  brightness 1000
fi

RUN=true

while $RUN; do 
  dwm-status & DWMSTATUS=$!
  wait "$DWMSTATUS"
done & STATUSLOOP=$!

while $RUN; do 
  dwm & DWM=$!
  wait "$DWM"
done

kill "$UNCLUTTER" "$STATUSLOOP" "$DWM"
~~~
