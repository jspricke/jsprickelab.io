# Large-Scale 3D Mapping with Explicit Loop Closing

by [Jochen Sprickerhof](https://jochen.sprickerhof.de/), [Andreas Nüchter](http://www.nuechti.de/), [Kai Lingemann](https://www.informatik.uni-osnabrueck.de/lingeman/), and [Joachim Hertzberg](https://www.informatik.uni-osnabrueck.de/hertzberg/)

->![Large-Scale 3D Mapping with Explicit Loop Closing](controlflow.png)<-

Simultaneous Localization and Mapping (SLAM) is the problem of building a map of an unknown environment by a mobile robot while at the same time navigating the environment, using the unfinished map.
For SLAM, two tasks have to be solved: First reliable feature extraction and data association, second the optimal estimation of poses and features.
These two parts are often referred to as SLAM frontend and backend.
Algorithms that solve SLAM by using laser scans commonly rely on matching closest points in the frontend part.
Then the SLAM front- and backend have to be iterated to ensure that the map converges.

This paper presents a novel approach for solving SLAM using 3D laser range scans.
We aim at avoiding the iteration between the SLAM front- and backend and propose a novel explicit loop closing heuristic (ELCH).
It *dissociates* the last scan of a sequence of acquired scans, reassociates it to the map, built so far by scan registration, and distributes the difference in the pose error over the SLAM graph.
We describe ELCH in the context of SLAM with 3D scans considering 6 DoF.
The performance is evaluated using ground truth data of an urban environment.

## Videos

An animated comparison between ELCH and LUM can be seen in the video.
It compares our previous strategy LUM (on the left) with our new strategy ELCH.
It shows the different steps during computation es depicted in the Figure above and the elapsed computing time.

<video width="1000" height="460" controls="controls" poster="unconv.png">
<source src="unconv.webm" type="video/webm">
<source src="unconv.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>

<video width="721" height="576" controls="controls" poster="han1.png">
<source src="han1.webm" type="video/webm">
<source src="han1.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>

## Gernerated 3D Map

Here you find more 3D views of the final map.

In the following the left image presents the 3D map before loop closing, while the right shows the corrected, consistent view:

[![3D view before loop closing](3Dpre.png =500x)](3Dpre.png)[![3D view after loop closing](3Dpost.png =500x)](3Dpost.png)

## Acknowledgements

We thank Oliver Wulf and Bernardo Wagner (both Leibniz University of Hannover, Germany) for making the 3D data set Hannover2 publicly available and Claus Brenner (Leibniz University of Hannover) for letting us use the airborne 3D data.

## References

  - Jochen Sprickerhof, Kai Lingemann, Andreas Nüchter, Joachim Hertzberg.  
    **A Heuristic Loop Closing Technique for Large-Scale 6D SLAM**.  
    *Automatika - Journal for Control, Measurement, Electronics, Computing and Communications*, 2011.  
    [[Paper](/Publications/Sprickerhof_automatika2011.pdf)], [[doi:10.1080/00051144.2011.11828420](https://doi.org/10.1080/00051144.2011.11828420)].
<details><summary>BibTeX</summary><pre>@article{automatika2011,
 author = {Jochen Sprickerhof and Kai Lingemann and Andreas Nüchter and Joachim Hertzberg},
 date = {2011},
 doi = {10.1080/00051144.2011.11828420},
 file = {Sprickerhof_automatika2011.pdf},
 issuetitle = {Special Issue Selected papers from the 4th European Conference on Mobile Robotics},
 journaltitle = {Automatika - Journal for Control, Measurement, Electronics, Computing and Communications},
 number = {3},
 pages = {199-222},
 title = {A Heuristic Loop Closing Technique for Large-Scale 6D SLAM},
 volume = {52}
}
</pre></details>
  - Jochen Sprickerhof, Andreas Nüchter, Kai Lingemann, Joachim Hertzberg.  
    **An Explicit Loop Closing Technique for 6D SLAM**.  
    *Proceedings of the 4th European Conference on Mobile Robotics (ECMR 2009)*, Mlini/Dubrovnik, Croatia, 2009-09.  
    [[Paper](/Publications/Sprickerhof_ecmr2009.pdf)].
<details><summary>BibTeX</summary><pre>@inproceedings{ecmr2009,
 author = {Jochen Sprickerhof and Andreas Nüchter and Kai Lingemann and Joachim Hertzberg},
 booktitle = {Proceedings of the 4th European Conference on Mobile Robotics (ECMR 2009)},
 date = {2009-09},
 file = {Sprickerhof_ecmr2009.pdf},
 location = {Mlini/Dubrovnik, Croatia},
 pages = {229-234},
 title = {An Explicit Loop Closing Technique for 6D SLAM}
}
</pre></details>
  - Jochen Sprickerhof.  
    **Effizientes Schleifenschließen mit sechs Freiheitsgraden in Laserscans von mobilen Robotern** Die ELCH (Explicit Loop Closing) Heuristik.  
    *Diploma Thesis*, 2009-08.  
    [[Paper](/Publications/Sprickerhof_thesis.pdf)], [[Link](https://jochen.sprickerhof.de/Software/ELCH/)].
<details><summary>BibTeX</summary><pre>@thesis{Sprickerhof2009b,
 author = {Jochen Sprickerhof},
 date = {2009-08},
 file = {Sprickerhof_thesis.pdf},
 institution = {Osnabrück University},
 langid = {german},
 pages = {65},
 subtitle = {Die ELCH (Explicit Loop Closing) Heuristik},
 title = {Effizientes Schleifenschließen mit sechs Freiheitsgraden in Laserscans von mobilen Robotern},
 type = {Diploma Thesis},
 url = {https://jochen.sprickerhof.de/Software/ELCH/}
}
</pre></details>
  - Dorit Borrmann, Jan Elseberg, Kai Lingemann, Andreas Nüchter, Joachim Hertzberg.  
    **Globally consistent 3D Mapping with Scan Matching**.  
    *Journal of Robotics and Autonomous Systems*, 2008-02.  
    [[Paper](http://kos.informatik.uos.de/download/Bachelorarbeit_Dorit_Jan_2006.pdf)], [[doi:10.1016/j.robot.2007.07.002](https://doi.org/10.1016/j.robot.2007.07.002)].
<details><summary>BibTeX</summary><pre>@article{ras2007,
 author = {Dorit Borrmann and Jan Elseberg and Kai Lingemann and Andreas Nüchter and Joachim Hertzberg},
 date = {2008-02},
 doi = {10.1016/j.robot.2007.07.002},
 file = {http://kos.informatik.uos.de/download/Bachelorarbeit_Dorit_Jan_2006.pdf},
 journaltitle = {Journal of Robotics and Autonomous Systems},
 number = {2},
 pages = {130-142},
 title = {Globally consistent 3D Mapping with Scan Matching},
 volume = {56}
}
</pre></details>
  - Dorit Borrmann, Jan Elseberg.  
    **Global konsistente 3D Kartierung am Beispiel des Botanischen Gartens in Osnabrück**.  
    *Bachelor thesis*, 2006-10.  
    [[Paper](http://kos.informatik.uos.de/download/Bachelorarbeit_Dorit_Jan_2006.pdf)].
<details><summary>BibTeX</summary><pre>@thesis{duj,
 author = {Dorit Borrmann and Jan Elseberg},
 date = {2006-10},
 file = {http://kos.informatik.uos.de/download/Bachelorarbeit_Dorit_Jan_2006.pdf},
 institution = {Osnabrück University},
 langid = {german},
 title = {Global konsistente 3D Kartierung am Beispiel des Botanischen Gartens in Osnabrück},
 type = {Bachelor thesis}
}
</pre></details>
