#!/usr/bin/python3
#
# simple converter from Biblatex to Markdown
#
# Copyright (C) 2019  Jochen Sprickerhof
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from sys import argv

from bibtexparser.bparser import BibTexParser
from bibtexparser.bwriter import BibTexWriter
from dateutil.parser import parse


def main():
    parser = BibTexParser(ignore_nonstandard_types=False)
    writer = BibTexWriter()
    writer.entry_separator = ""

    bib = parser.parse_file(open(argv[1]))
    db = {}
    for entry in bib.entries:
        if "date" in entry:
            date = "date"
        elif "year" in entry:
            date = "year"
        else:
            raise ValueError(f"publication does neither contain date nor year: {entry}")
        db.setdefault(parse(entry[date]).year, []).append(entry)

    template = """\
  - {author}.  
    {title}.  
    *{booktitle}*, {conf}.  """

    out = []
    for year in sorted(db.keys(), reverse=True):
        out.append(f"# {year}")

        elem = []
        for pub in db[year]:
            fmt = {}

            fmt["author"] = pub["author"].replace(" and", ",")

            fmt["title"] = f'**{pub["title"]}**'
            if "subtitle" in pub:
                fmt["title"] = f'**{pub["title"]}** {pub["subtitle"]}'

            if "booktitle" in pub:
                fmt["booktitle"] = pub["booktitle"]
            elif "journaltitle" in pub:
                fmt["booktitle"] = pub["journaltitle"]
            elif "journal" in pub:
                fmt["booktitle"] = pub["journal"]
            elif "type" in pub:
                fmt["booktitle"] = pub["type"]
            else:
                raise ValueError(
                    f"publication does contain none of booktitle, journaltitle, journal, type: {pub}"
                )

            if "date" in pub:
                date = "date"
            elif "year" in pub:
                date = "year"

            fmt["conf"] = ", ".join(
                [pub[k] for k in ("note", "location", date) if k in pub]
            )

            elem.append(template.format(**fmt).replace("{", "").replace("}", ""))

            ref = []
            if "file" in pub:
                ref.append(f'[[Paper]({pub["file"]})]')
            if "doi" in pub:
                ref.append(f'[[doi:{pub["doi"]}](https://doi.org/{pub["doi"]})]')
            if "url" in pub:
                ref.append(f'[[Link]({pub["url"]})]')
            if ref:
                elem.append("    {}.".format(", ".join(ref)))

            elem.append(
                f"<details><summary>BibTeX</summary><pre>{writer._entry_to_bibtex(pub)}</pre></details>"
            )

        out.append("\n".join(elem))
    print("\n\n".join(out))


if __name__ == "__main__":
    main()
