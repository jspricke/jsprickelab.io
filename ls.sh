#!/bin/sh -e
#
# ls -- List Site (because the web doesn't have one)
#
# Copyright (C) 2017-2019  Jochen Sprickerhof
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

IN="site"
OUT="public"
SITENAME="Jochen Sprickerhof"
DESCRIPTION="Jochen Sprickerhof's home page"
KEYWORDS="Jochen Sprickerhof, Open Source Software, Debian, F-Droid, ROS, Sensor Data Interpretation, Autonomous Mobile Robots, Artificial Intelligence"
LANG="en"

list() {
	[ "$(find "$1"/ -maxdepth 1 -name '*.lnk' -o -name '*.md' -o -name '*.bib' | wc -l)" = 1 ] && return
	printf '<p>\n'
	[ "$1" = "" ] && printf '<a href="/">⌂</a>\n'
	for l in "$1"*; do
	  [ "$l" = "$s" ] && THIS=' id="thisSite"' || THIS=''
		case $l in
			*index.*)
				;;
			*.lnk)
				printf '<a href="%s">%s→</a>\n' "$(cat -- "$l")" "$(basename -- "$l" .lnk)"
				;;
			*.md|*.bib)
				printf '<a href="/%s"%s>%s</a>\n' "${l%.*}.html" "$THIS" "$(basename -- "$l" ".${l##*.}")"
				;;
			*)
				[ -d "$l" ] && printf '<a href="/%s/"%s>%s/</a>\n' "$l" "$THIS" "$(basename -- "$l")"
				;;
		esac
	done
	printf '</p>\n'
}

html() {
	printf '<!DOCTYPE html><html lang="%s"><head>\n<meta charset="utf-8">\n' "$LANG"
	case "$s" in
		index.md)
			printf '<title>%s</title>\n' "$SITENAME"
			;;
		*/index.*)
			printf '<title>%s | %s</title>\n' "$(basename -- "$1")" "$SITENAME"
			;;
		*)
			printf '<title>%s | %s</title>\n' "$(basename -- "$s" ".${s##*.}")" "$SITENAME"
			;;
	esac
	printf '<link rel="stylesheet" type="text/css" href="/style.css">\n'
	printf '<meta name="viewport" content="width=device-width, initial-scale=1">\n'
	printf '<meta name="description" content="%s">\n' "$DESCRIPTION"
	printf '<meta name="keywords" content="%s">\n' "$KEYWORDS"
	case "$s" in
		index.md)
			printf '</head><body>\n<header>%s</header>\n' "$SITENAME"
			;;
		*)
			printf '</head><body>\n<header><a href="/">%s</a></header>\n' "$SITENAME"
			;;
	esac
	printf '<nav>'
	printf '%s\n' "$2"
	list "$1"
	printf '</nav>'
	printf '<div id="changed">Last changed: <a href="%s">%s</a></div>\n' "$(git remote get-url origin | sed -e 's#//[^@]*@#//#' -e 's/.git$//')/-/commits/master/site/$s" "$(git log -1 --format="%ad" --date=short -- "$s")"
	printf '<main>\n'
	case "$s" in
		*.md)
			cat -- "$s"
			;;
		*.bib)
			../bib2md.py "$s"
			;;
	esac | markdown -ffencedcode
	printf '</main></body></html>\n'
}

site() {
	mkdir -p "../$OUT/$1"
	for s in "$1"*; do
		[ -d "$s" ] && site "$s"/ "$2$(list "$1")" && continue
		case $s in
			*.lnk)
				;;
			*.md|*.bib)
				html "$@" > "../$OUT/${s%.*}.html"
				;;
			*)
				cp -l -- "$s" "../$OUT/$s"
				;;
		esac
	done
	cp -lr -- "$1".[!.]* "../$OUT/$1" 2> /dev/null || true
}

cd "$IN" && site
