# ls -- List Site

## because the web doesn't have one

Simple static site generator.

## How to use

Clone the repository and adopt the content.
There is some meta data in the `ls.sh`, the rest is in the `site/` directory.
For Gitlab, there is a `.gitlab-ci.yml` to automatically deploy it, if you enable Gitlab pages.

## Try it locally

~~~
apt install discount python3-dateutil python3-bibtexparser
./ls.sh; cd public; python3 -m http.server & open http://localhost:8000
~~~
